<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use HasFactory;

    //1:M relation
    public function propertyFeatures(){
        return $this->hasMany(PropertyFeature::class);
    }
}
