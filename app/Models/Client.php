<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    //1:M relation
    public function contracts(){
        return $this->hasMany(Contract::class);
    }

    //1:M relation
    public function maintaincRequests(){
        return $this->hasMany(MaintainceRequest::class);
    }
}
