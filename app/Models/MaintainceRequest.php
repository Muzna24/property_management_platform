<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintainceRequest extends Model
{
    use HasFactory;

    //M:1 relation
    public function client(){
        return $this->belongsTo(Client::class);
    }

    //M:1 relation
    public function property(){
        return $this->belongsTo(Property::class);
    }
}
