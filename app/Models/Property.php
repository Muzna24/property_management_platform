<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    //1:M relation
    public function contracts(){
        return $this->hasMany(Contract::class);
    }

    //1:M relation
    public function flats(){
        return $this->hasMany(Flat::class);
    }

    //1:M relation
    public function homes(){
        return $this->hasMany(Home::class);
    }

    //1:M relation
    public function rooms(){
        return $this->hasMany(Room::class);
    }

    //1:M relation
    public function propertyAttachments(){
        return $this->hasMany(PropertyAttachment::class);
    }

    //M:1 relation
    public function propertGroup(){
        return $this->belongsTo(PropertyGroup::class);
    }

    //1:M relation
    public function maintainceRequests(){
        return $this->hasMany(MaintainceRequest::class);
    }

    //1:M relation
    public function cities(){
        return $this->hasMany(City::class);
    }

    //1:M relation
    public function propertyFeatures(){
        return $this->hasMany(PropertyFeature::class);
    }

    //M:1 relation
    public function user(){
        return $this->belongsTo(User::class);
    }
}
