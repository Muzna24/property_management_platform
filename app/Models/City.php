<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    //M:1 relation
    public function state(){
        return $this->belongsTo(State::class);
    }

    //M:1 relation
    public function property(){
        return $this->belongsTo(Property::class);
    }
}
