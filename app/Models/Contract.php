<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;

    //M:1 relation
    public function client(){
        return $this->belongsTo(Client::class);
    }

    //1:M relation
    public function contractPyments(){
        return $this->hasMany(ContractPayment::class);
    }

    //1:M relation
    public function contractDetails(){
        return $this->hasMany(ContractDetail::class);
    }

    //M:1 relation
    public function property(){
        return $this->belongsTo(Property::class);
    }
}
