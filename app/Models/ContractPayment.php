<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContractPayment extends Model
{
    use HasFactory;

    //M:1 relation
    public function contract(){
        return $this->belongsTo(Contract::class);
    }
}
