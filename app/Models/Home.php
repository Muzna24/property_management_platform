<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    //M:1 relation
    public function property(){
        return $this->belongsTo(Property::class);
    }
}
