<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->string('status');
            $table->double('location_long');
            $table->double('location_lat');
            $table->boolean('admin_approval');
            $table->double('daily_price');
            $table->double('monthly_price');
            $table->double('annual_price');
            $table->boolean('electricity_included');
            $table->boolean('water_included');
            $table->boolean('wifi_included');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('propertygroup_id');
            $table->unsignedInteger('city_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
};
